import React, { Component } from 'react';

class News extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showText: this.props.flag
        };
    }

    render() {
        return (
            <>
                <h2>{this.props.news.headline}</h2>
                <img src={this.props.news.img} alt="" />
                <p>{this.state.showText ? null : this.props.news.shortDescription}</p>
                <p>{this.state.showText ? this.props.news.text : null}</p>
                <button onClick={this.showButton}>{this.state.showText ? "Hide" : "Show"}</button>
            </>
        )
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps.flag !== this.state.showText) {
            this.setState({
                showText: nextProps.flag
            });
        }
    }
    
    showButton = () => {
        this.setState({
            showText: !this.state.showText
        })
    }
}

export default News;
