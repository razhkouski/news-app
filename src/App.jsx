import React, { Component } from 'react';
import News from './NewsClass';
import { news } from './news';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newsArr: news,
            desiredValue: ''
        };
    }

    render() {
        const newsList = this.state.newsArr.map((news, i) => i === 0 ? <News key={news.id} news={news} index={i} flag/> : <News key={news.id} news={news} index={i}/>);
        
        // {this.state.newsArr.map((item, i) =>  i === 0 ?  <NewsItem key={item.id} news={item} flag/> : <NewsItem key={item.id} news={item}/>)}

        return (
            <>
                <h1>Football news</h1>
                <input onChange={this.handleChange} value={this.state.desiredValue}></input>
                <button onClick={this.findNews}>Find</button>
                <button onClick={this.reverseNews}>Revert</button>
                <button onClick={this.resetNews}>Reset</button>
                {newsList}
            </>
        )
    }

    handleChange = (event) => {
        this.setState({
            desiredValue: event.target.value
        })
    }

    findNews = () => {
        this.setState({
            newsArr: this.state.newsArr.filter((elem) => elem.headline.toLowerCase() === this.state.desiredValue.toLowerCase()),
            desiredValue: ''
        })
    }

    reverseNews = () => {
        this.setState({
            newsArr: this.state.newsArr.reverse()
        });
    }

    resetNews = () => {
        this.setState({
            newsArr: news
        })
    }
}

export default App;