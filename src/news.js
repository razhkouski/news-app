export const news = [
    {
        id: 1,
        headline: 'Rangers to face Malmo in Champions League qualifier',
        img: `https://e0.365dm.com/21/07/1600x900/skysports-steven-gerrard-rangers_5456173.jpg?20210723125355`,
        shortDescription: `Rangers will face Malmo in the Champions League qualifiers after the Swedes saw off HJK Helsinki.`,
        text: `Rangers will face Malmo in the Champions League qualifiers after the Swedes saw off HJK Helsinki.
        Malmo secured a 2-2 second-leg draw in the Finnish capital to go through 4-3 on aggregate.
        Rangers will travel to Sweden next week and host Malmo at Ibrox in the second leg of the third qualifying round on August 10.
        The Gers, who begin their Champions League journey in the third round of qualifying after winning the Scottish Premiership title, have not made the group stages of Europe's elite competition since 2010.
        They are only four games away from achieving that goal.
        Should Rangers beat Malmo over two legs, the Ibrox side would progress to the Champions League play-off match, which is one step away from the group stages.`
    },
    {
        id: 2,
        headline: `Consortium 'remains committed' to West Ham takeover`,
        img: `https://e0.365dm.com/21/07/1600x900/skysports-west-ham-general_5460368.jpg?20210727210328`,
        shortDescription: `The head of a consortium attempting a takeover of West Ham insists he remains committed to buying the club.`,
        text: `The head of a consortium attempting a takeover of West Ham insists he remains committed to buying the club.
        However, the PA news agency understands Hammers owners David Sullivan and David Gold have no intention of selling.
        Sullivan said last week that no formal offers have been made, and that stance was unchanged on Tuesday night.                  
        But Philip Beard, the former QPR chief executive who is fronting the bid, has rejected that claim and insists his consortium met the asking price and provided proof of funds when they made the approach in February.                    
        "It is not my intention to engage in a public debate on the comments made by David Sullivan last week but, upon advice, it is necessary to correct unhelpful and inaccurate statements," Beard said in a statement. 
        "In response to David Sullivan's comments about the nature and value of the bid put forward by the consortium, I can confirm that a formal offer was made which was in fact the figure that David Sullivan had initially asked for.                    
        "Our city lawyers were instructed and David Sullivan was provided with a proof of funds.`
    },
    {
        id: 3,
        headline: `Rangers, Celtic receive attendance boost ahead of new season`,
        img: `https://e0.365dm.com/21/07/1600x900/skysports-rangers-ibrox_5460358.jpg?20210727204343`,
        shortDescription: `Rangers and Celtic have been handed attendance boosts ahead of their opening games at Ibrox and Parkhead respectively.`,
        text: `Rangers and Celtic have been handed attendance boosts ahead of their opening games at Ibrox and Parkhead respectively.
        Scottish champions Rangers will have an extra 6,000 tickets for Saturday's Premiership opener against Livingston, making a total of 23,000 seats available for season ticket holders.                    
        The move comes after a steady increase in crowds allowed for three pre-season friendlies at Ibrox, which peaked with 12,500 for Rangers' victory over Real Madrid on Sunday.
        And Celtic will have 24,500 supporters inside their ground for their opening home match against Dundee on Saturday August 7 but there will be no Hoops fans inside Tynecastle for their season kick-off.
        Standard crowd limits for outdoor events under the Scottish Government's Level 0 coronavirus restrictions are 2,000 but organisers can apply to their local authority for the right to host bigger attendances.
        The different approaches from councils are leading to disappointments for some clubs, notably Hearts.
        The Jambos will only have 4,535 season ticket holders inside Tynecastle for their return to the top flight against Celtic following a decision by Edinburgh City Council.`
    },
    {
        id: 4,
        headline: `Man Utd confirm deal with Real Madrid for Varane`,
        img: `https://e0.365dm.com/21/07/1600x900/skysports-raphael-varane-real-madrid_5445086.jpg?20210712143547`,
        shortDescription: `Manchester United have confirmed that they have reached an agreement with Real Madrid to sign defender Raphael Varane.`,
        text: `Manchester United have confirmed they have reached an agreement with Real Madrid to sign defender Raphael Varane.
        The price for the France international is believed to be £41m including add-ons.
        A club statement said: "Manchester United is delighted to announce the club has reached agreement with Real Madrid for the transfer of French international defender and World Cup winner, Raphaël Varane, subject to a medical and to player terms being finalised."
        Having already signed England international Jadon Sancho for £73m from Borussia Dortmund, the acquisition of Varane represents a further statement of intent from United.
        Ole Gunnar Solskjaer's team were second in the Premier League last season but finished 12 points behind Manchester City.
        Varane is well established as one of the world's top defenders, having joined Real from French side Lens in the summer of 2011.
        He has gone on to make over 350 appearances for the club, and be capped 79 times by France, helping them to World Cup glory in 2018.`
    },
    {
        id: 5,
        headline: `No police action over Rooney blackmail complaint`,
        img: `https://e0.365dm.com/21/05/1600x900/skysports-wayne-rooney-rooney_5374383.jpg?20210508154010`,
        shortDescription: `Cheshire Police will not take action against a complaint of blackmail made regarding online images of Wayne Rooney.`,
        text: `Cheshire Police will not take action against a complaint of blackmail made regarding online images of Wayne Rooney.
        Rooney's representatives contacted police over what they believe was an illicit attempt to entrap the Derby County manager with a range of explicit photographs. 
        The images went viral on social media on Sunday, appearing to show the former Manchester United and England player asleep in a hotel room surrounded by a group of young women. 
        There is no suggestion Rooney acted inappropriately, or even he was aware of what was going on around him.
        However, Cheshire Police said on Tuesday no action would be taken after speaking to the person involved.
        The statement read: "On Monday 26 July Cheshire Constabulary received reports of a possible blackmail relating to a number of images circulating online.
        "Officers have spoken to the person involved and are satisfied that no offences have taken place. The person involved has also stated that they do not wish to take the matter any further."
        Rooney is about to start his first full season in charge at Derby, having narrowly kept them in the Championship last season after taking over from Phillip Cocu. 
        However, he has warned he will be unable to field a team unless the club makes significant signings. 
        Derby currently have only nine registered senior players, two of whom are goalkeepers. 
        One of those players Jason Knight was injured after a 50-50 tackle in training with Rooney. He has been ruled out for three months.`
    }
];